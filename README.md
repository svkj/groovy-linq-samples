
# Groovy LINQ Samples

In the spirit of [Kotlin LINQ Samples](https://github.com/mythz/kotlin-linq-examples), or [Java LINQ Samples](https://github.com/mythz/java-linq-examples) or [C# LINQ Samples](http://code.msdn.microsoft.com/101-LINQ-Samples-3fb9811b), this repo contains the groovy syntax for a bunch of LINQ operators.  See [AppTest.groovy](https://gitlab.com/svermeulen/groovy-linq-samples/blob/master/src/test/groovy/groovy/linq/samples/AppTest.groovy) for the code.

I haven't included anywhere near the 101 samples from those other pages yet so PR welcome



