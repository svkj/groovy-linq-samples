package groovy.linq.samples

import spock.lang.Specification

class AppTest extends Specification {
  def "linq where"() {
    setup:
    def list1 = [1, 2, 3, 4]
    when:
    def list2 = list1.findAll { it > 2 }
    then:
    list2 == [3, 4]
  }

  def "linq select"() {
    setup:
    def list1 = [1, 2, 3]
    when:
    def list2 = list1.collect { it + 1 }
    then:
    list2 == [2, 3, 4]
  }

  def "linq select many"() {
    setup:
    def list1 = [[1, 2], [3, 4]]
    when:
    def list2 = list1.collectMany { it }
    then:
    list2 == [1, 2, 3, 4]
  }

  def "linq take"() {
    setup:
    def list1 = [1, 2, 3, 4]
    when:
    def list2 = list1.take(2)
    then:
    list2 == [1, 2]
  }

  def "linq skip"() {
    setup:
    def list1 = [1, 2, 3, 4]
    when:
    def list2 = list1.drop(2)
    then:
    list2 == [3, 4]
  }

  def "linq takeWhile"() {
    setup:
    def list1 = [1, 2, 3, 4]
    when:
    def list2 = list1.takeWhile { it < 3 }
    then:
    list2 == [1, 2]
  }

  def "linq skipWhile"() {
    setup:
    def list1 = [1, 2, 3, 4]
    when:
    def list2 = list1.dropWhile { it < 3 }
    then:
    list2 == [3, 4]
  }

  def "linq orderBy"() {
    setup:
    def list1 = [4, 2, 3, 6, 1]
    when:
    def list2 = list1.sort { it }
    then:
    list2 == [1, 2, 3, 4, 6]
  }

  def "linq orderByDescending"() {
    setup:
    def list1 = [4, 2, 3, 6, 1]
    when:
    def list2 = list1.sort { it }
    list2.reverse(true)
    then:
    list2 == [6, 4, 3, 2, 1]
  }

  def "linq orderBy thenBy"() {
    setup:
    def list1 = [[4, 1], [1, 0], [4, 0], [1, 2]]
    when:
    def list2 = list1.sort { a, b ->
      if (a[0] == b[0]) {
        return a[1] <=> b[1]
      }

      return a[0] <=> b[0] 
    }
    then:
    list2 == [[1, 0], [1, 2], [4, 0], [4, 1]]
  }

  def "linq distinct"() {
    setup:
    def list1 = [1, 1, 2, 3, 3]
    when:
    def list2 = list1.unique()
    then:
    list2 == [1, 2, 3]
  }

  def "linq union"() {
    setup:
    def list1 = [1, 2, 3]
    def list2 = [3, 4, 5]
    when:
    def list3 = list1 + list2
    then:
    list3 == [1, 2, 3, 3, 4, 5]
  }

  def "linq intersect"() {
    setup:
    def list1 = [1, 2, 3]
    def list2 = [3, 4, 5]
    when:
    def list3 = list1.intersect(list2)
    then:
    list3 == [3]
  }

  def "linq except"() {
    setup:
    def list1 = [1, 2, 3]
    when:
    def list2 = list1.findAll { it != 2 }
    then:
    list2 == [1, 3]
  }

  def "linq toDictionary"() {
    setup:
    def list1 = [1, 2, 3]
    when:
    def map = list1.collectEntries { [it, it * 2] }
    then:
    map[1] == 2
    map[2] == 4
    map[3] == 6
  }

  def "linq single"() {
    setup:
    def list1 = [1, 2]
    when:
    // There is no Single() equivalent so we need to use an explicit assert instead
    assert list1.size() == 1
    def value = list1.first()
    then:
    thrown(Error)
  }

  def "linq first"() {
    setup:
    def list1 = [1, 2, 3]
    when:
    def value = list1.first()
    then:
    value == 1
  }

  def "linq first"() {
    setup:
    def list1 = []
    when:
    def value = list1.first()
    then:
    thrown(NoSuchElementException)
  }

  def "linq firstOrDefault"() {
    setup:
    def list1 = []
    when:
    def value = list1[0]
    then:
    value == null
  }

  def "linq range"() {
    when:
    def list1 = 1..5
    then:
    list1.size() == 5
    list1[0] == 1
    list1[1] == 2
  }

  def "linq any"() {
    when:
    def list1 = 1..5
    then:
    list1.any { it > 2 }
    !list1.any { it > 5 }
  }

  def "linq all"() {
    when:
    def list1 = 1..5
    then:
    list1.every { it > 0 }
    !list1.every { it > 2 }
  }

  def "linq sum"() {
    setup:
    def list1 = 1..3
    when:
    def value = list1.sum()
    then:
    value == 6
  }

  def "linq min"() {
    setup:
    def list1 = 1..3
    when:
    def value = list1.min()
    then:
    value == 1
  }

  def "linq max"() {
    setup:
    def list1 = 1..3
    when:
    def value = list1.max()
    then:
    value == 3
  }
}
